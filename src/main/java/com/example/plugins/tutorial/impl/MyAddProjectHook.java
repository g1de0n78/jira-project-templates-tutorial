package com.example.plugins.tutorial.impl;

import com.atlassian.jira.project.template.hook.AddProjectHook;
import com.atlassian.jira.project.template.hook.ConfigureData;
import com.atlassian.jira.project.template.hook.ConfigureResponse;
import com.atlassian.jira.project.template.hook.ValidateData;
import com.atlassian.jira.project.template.hook.ValidateResponse;

public class MyAddProjectHook implements AddProjectHook {

	@Override
	public ConfigureResponse configure(ConfigureData arg0) {
        return ConfigureResponse.create();
	}

	@Override
	public ValidateResponse validate(ValidateData arg0) {
        ValidateResponse validateResponse = ValidateResponse.create();
        if (arg0.projectKey().equals("TEST"))
        {
            validateResponse.addErrorMessage("Invalid Project Key");
        }

        return validateResponse;
	}

}
